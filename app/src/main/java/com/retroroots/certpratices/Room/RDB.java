package com.retroroots.certpratices.Room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {REntity.class}, version = 1, exportSchema = false)
public abstract class RDB extends RoomDatabase
{
    private static RDB instance;

    public abstract RDAO dao();

    public static synchronized RDB GetInstance(Context context)
    {
        if (instance == null)
            instance = Room.databaseBuilder(context, RDB.class, "PrefDB")
                           .fallbackToDestructiveMigration() //We need this or else the app will crash when the db scheme changes without a migration.
                           .build();

        return instance;

    }
}
