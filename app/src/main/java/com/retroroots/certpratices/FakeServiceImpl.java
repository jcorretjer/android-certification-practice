package com.retroroots.certpratices;

public class FakeServiceImpl implements IFakeService
{
    @Override
    public int Add(int num1, int num2)
    {
        return num1 + num2;
    }

    public FakeServiceImpl()
    {
        IFakeService iFakeService = new IFakeService()
        {
            @Override
            public int Add(int num1, int num2)
            {
                return 0;
            }
        };
    }
}
