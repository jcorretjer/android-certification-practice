package com.retroroots.certpratices.RecyclerView.Paging;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.retroroots.certpratices.R;
import com.retroroots.certpratices.Room.REntity;

public class PAdapter extends PagedListAdapter<REntity, PAdapter.PageViewHolder>
{
    public PAdapter()
    {
        super(DIFF_CALLBACK);
    }

    public static class PageViewHolder extends RecyclerView.ViewHolder
    {
        private TextView titleTxtVw;

        public PageViewHolder(@NonNull View itemView)
        {
            super(itemView);

            titleTxtVw = itemView.findViewById(R.id.recTitle);
        }
    }

    @NonNull
    @Override
    public PageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new PAdapter.PageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.child_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PageViewHolder holder, int position)
    {
        holder.titleTxtVw.setText("Title " + getItem(position).getIndex());
    }

    private final static DiffUtil.ItemCallback<REntity> DIFF_CALLBACK = new DiffUtil.ItemCallback<REntity>()
    {
        @Override
        public boolean areItemsTheSame(@NonNull REntity oldItem, @NonNull REntity newItem)
        {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull REntity oldItem, @NonNull REntity newItem)
        {
            return oldItem.getIndex() == newItem.getIndex();
        }
    };

    /*private final static DiffUtil.ItemCallback<Integer> DIFF_CALLBACK = new DiffUtil.ItemCallback<Integer>()
    {
        @Override
        public boolean areItemsTheSame(@NonNull Integer oldItem, @NonNull Integer newItem)
        {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Integer oldItem, @NonNull Integer newItem)
        {
            return oldItem.equals(newItem);
        }
    };*/
}
