package com.retroroots.certpratices;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/*
* Mockito is useful for when:
* -Test throws class not mocked error
* -I need real instance of an object with real values to something and I can't get it through a test.
* -If I need to run a test using a method that doesn't exist, but I assume will work once it's been implemented.
* -Or if I need a value from a method, but I don't wish to implement the method.
* -If I need to run an implementation I created of a specific class or any other existing class I can just spy it
*/
@RunWith(MockitoJUnitRunner.class)
public class ServiceTest
{
    @Mock
    IFakeService iFakeService;

    @Test
    public void MockExample1()
    {
        when(iFakeService.Add(1, 2)).thenReturn(3);

        int num = iFakeService.Add(1, 2);

        assertEquals(3, num);
    }

    @Test
    public void MockTest2()
    {
        FakeServiceImpl fakeService = Mockito.mock(FakeServiceImpl.class);

        int num = fakeService.Add(1,2);

        //Won't pass because returns 0 because it's executing the default implementation instead of the one that was actually implemented in the class
        assertEquals(3, num);
    }

    @Test
    public void MockTest3()
    {
        FakeServiceImpl fakeService = Mockito.mock(FakeServiceImpl.class);

        int num = fakeService.Add(1, 2);

        //If we verify we can see it's not actually running our implementation. Just the mocked empty implementation
        Mockito.verify(fakeService).Add(1, 2);

        //Returns 0 because it's executing the default implementation instead of the one that was actually implemented in the class
        assertEquals(3, num);
    }

    @Test
    public void SpyTest()
    {
        //Spy creates an actual mock using our written implementation instead of the default
        FakeServiceImpl fakeService = Mockito.spy(FakeServiceImpl.class);

        int num = fakeService.Add(1,2);

        //Doesn't return error because it is actually running our implementation.
        Mockito.verify(fakeService).Add(1, 2);

        assertEquals(3, num);
    }
}
