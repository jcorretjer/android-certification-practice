package com.retroroots.certpratices;

import android.app.Application;

import com.retroroots.certpratices.Room.RRepo;

public class APP extends Application
{
    public static RRepo rRepo;

    @Override
    public void onCreate()
    {
        super.onCreate();

        rRepo = new RRepo(this);
    }
}
