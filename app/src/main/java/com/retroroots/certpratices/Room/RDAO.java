package com.retroroots.certpratices.Room;

import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface RDAO
{
    @Insert
    public void Insert(REntity entity);

    @Query("SELECT * FROM AndroidCert ORDER BY `index` ASC")
    //public LiveData<List<REntity>> GetAll();
    public DataSource.Factory<Integer ,REntity> GetAll();
}
