package com.retroroots.certpratices;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.retroroots.certpratices.Models.Post;
import com.retroroots.certpratices.RecyclerView.Paging.PAdapter;
import com.retroroots.certpratices.RecyclerView.Paging.PDataSource;
import com.retroroots.certpratices.RecyclerView.Paging.DSFactory;
import com.retroroots.certpratices.RecyclerView.RAdapter;
import com.retroroots.certpratices.Room.REntity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
{
    private DrawerLayout drawerLayout;

    private final String API_TEST_URL = "https://cat-fact.herokuapp.com/";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((TextView)findViewById(R.id.txtvw)).setText("testing");

        //region Android Core

        Toast.makeText(this, "Test Toast", Toast.LENGTH_LONG)
             .show();

        Snackbar.make(findViewById(R.id.mainView), "Test snackbar", Snackbar.LENGTH_SHORT)
                .show();

        //region Notification

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            NotificationChannel notificationChannel = new NotificationChannel("0", "channelName", NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("channel description");

            notificationManager.createNotificationChannel(notificationChannel);
        }

        Notification notification = new NotificationCompat.Builder(this, "0")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Content title")
                .setContentText("content text")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();

        notificationManagerCompat.notify(1, notification);

        //endregion

        //region Work manager

        /*PeriodicWorkRequest request = new PeriodicWorkRequest.Builder(WorkIt.class, 1, TimeUnit.MINUTES)
                .addTag("workerTag")
                .build();*/

        OneTimeWorkRequest request = new OneTimeWorkRequest.Builder(WorkIt.class)
                .build();

        WorkManager workManager = WorkManager.getInstance(this);

        workManager.enqueue(request);

        workManager.getWorkInfoByIdLiveData(request.getId())
                   .observe(this, new Observer<WorkInfo>()
                   {
                       @Override
                       public void onChanged(WorkInfo workInfo)
                       {
                           if (workInfo.getState()
                                       .name()
                                       .equals("RUNNING"))
                               Toast.makeText(MainActivity.this, "Worker running", Toast.LENGTH_LONG)
                                    .show();

                           else if (workInfo.getState()
                                            .isFinished())
                               Toast.makeText(MainActivity.this, "Worker done", Toast.LENGTH_LONG)
                                    .show();
                       }
                   });

        //endregion

        //endregion

        //region UI

        //region Recycle view & paging lib

        RecyclerView recyclerView = findViewById(R.id.recView);

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //region Setup paging adapter

        /*PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(PDataSource.PAGE_SIZE)
                .build();*/

        //DSFactory pageFactory = new DSFactory();

        //LiveData<PagedList<Integer>> pagedListLiveData = new LivePagedListBuilder(pageFactory, config).build();

        final PAdapter recPageAdapter = new PAdapter();

        recyclerView.setAdapter(recPageAdapter);

        APP.rRepo.GetAll()
                 .observe(this, new Observer<PagedList<REntity>>()
                 {
                     @Override
                     public void onChanged(PagedList<REntity> rEntities)
                     {
                         if(rEntities.isEmpty())
                             InsertDummyRecords();

                         recPageAdapter.submitList(rEntities);
                     }
                 });

        //endregion

        //region Set up regular recyclerView adapter

        //recyclerView.setAdapter(recPageAdapter);

        //RAdapter recAdapter = new RAdapter(list);

        //final RAdapter REC_ADAPTER = new RAdapter(new ArrayList<REntity>());

        //recyclerView.setAdapter(REC_ADAPTER);

        /* APP.rRepo.GetAll().observe(this, new Observer<List<REntity>>()
        {
            @Override
            public void onChanged(List<REntity> rEntities)
            {
                REC_ADAPTER.SetList(rEntities);
            }
        });*/

        //endregion

        //endregion

        Toolbar toolbar = findViewById(R.id.mainToolbar);

        setSupportActionBar(toolbar);

        //region Navigation Drawer

        drawerLayout = findViewById(R.id.drawerLayout);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navDrawerOpen, R.string.navDrawerClose);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();

        NavigationView navigationView = findViewById(R.id.navView);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.item1Drawer:

                        Toast.makeText(MainActivity.this, "Item 1 selected", Toast.LENGTH_SHORT)
                             .show();
                        break;

                    case R.id.drawerSectItem1:

                        Toast.makeText(MainActivity.this, "Section Item 1 selected", Toast.LENGTH_SHORT)
                             .show();

                        break;
                }

                drawerLayout.closeDrawer(GravityCompat.START);

                return true;
            }
        });

        //endregion

        //InsertDummyRecords();

        //endregion

        //region Retrofit

        final Retrofit RETROFIT = new Retrofit.Builder()
                .baseUrl(API_TEST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RETROFIT.create(IJsonApi.class).getPost().enqueue(new Callback<List<Post>>()
        {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response)
            {
                if(!response.isSuccessful())
                    Log.i("API Result", String.valueOf(response.code()));

                final List<Post> POSTS = response.body();

                for (Post p: POSTS)
                {
                    Log.i("API Result " + p.getId(), p.toString());
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t)
            {
                Log.i("API Result", t.getMessage());
            }
        });

        //endregion

    }

    private void InsertDummyRecords()
    {
        APP.rRepo.Insert(new REntity(-1));
        APP.rRepo.Insert(new REntity(1));
        APP.rRepo.Insert(new REntity(2));
        APP.rRepo.Insert(new REntity(3));
        APP.rRepo.Insert(new REntity(4));
        APP.rRepo.Insert(new REntity(5));
        APP.rRepo.Insert(new REntity(6));
        APP.rRepo.Insert(new REntity(7));
        APP.rRepo.Insert(new REntity(8));
        APP.rRepo.Insert(new REntity(9));
        APP.rRepo.Insert(new REntity(10));
        APP.rRepo.Insert(new REntity(11));
        APP.rRepo.Insert(new REntity(12));
        APP.rRepo.Insert(new REntity(13));
        APP.rRepo.Insert(new REntity(14));
        APP.rRepo.Insert(new REntity(15));
        APP.rRepo.Insert(new REntity(16));
        APP.rRepo.Insert(new REntity(17));
        APP.rRepo.Insert(new REntity(18));
        APP.rRepo.Insert(new REntity(19));
        APP.rRepo.Insert(new REntity(20));
        APP.rRepo.Insert(new REntity(21));
        APP.rRepo.Insert(new REntity(22));
        APP.rRepo.Insert(new REntity(23));
    }

    @Override
    public void onBackPressed()
    {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }

    public static class WorkIt extends Worker
    {
        Context context;

        public WorkIt(@NonNull Context context, @NonNull WorkerParameters workerParams)
        {
            super(context, workerParams);

            this.context = context;
        }

        @NonNull
        @Override
        public Result doWork()
        {
            return Result.success();
        }
    }
}

