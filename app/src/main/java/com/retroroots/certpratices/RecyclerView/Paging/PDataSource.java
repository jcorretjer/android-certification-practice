package com.retroroots.certpratices.RecyclerView.Paging;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import java.util.ArrayList;
import java.util.List;

public class PDataSource extends PageKeyedDataSource<Integer, Integer>
{
    private List<Integer> list;

    public static final int PAGE_SIZE = 4;

    private int lastVisibleIndex = PAGE_SIZE;

    private int currentPage = 1;

    public PDataSource()
    {
        this.list = new ArrayList<>();

        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        list.add(15);
        list.add(16);
        list.add(17);
        list.add(18);
        list.add(19);
        list.add(20);
        list.add(21);
        list.add(22);
        list.add(23);
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Integer> callback)
    {
        lastVisibleIndex = PAGE_SIZE;

        List<Integer> l = list.subList(0, lastVisibleIndex);

        lastVisibleIndex = l.size();

        callback.onResult(l, null, currentPage);
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Integer> callback)
    {
        List<Integer> l = list;

        Integer key = null;

        if((lastVisibleIndex - PAGE_SIZE) > 0)
        {
            l = list.subList((lastVisibleIndex - PAGE_SIZE), ((lastVisibleIndex - PAGE_SIZE)) + PAGE_SIZE);

            lastVisibleIndex -= l.size() - 1;

            key = params.key - 1;
        }

        callback.onResult(l, key);
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Integer> callback)
    {
        List<Integer> l = list;

        Integer key = null;

        if(lastVisibleIndex < list.size())
        {
            l = list.subList(lastVisibleIndex, lastVisibleIndex + PAGE_SIZE);

            lastVisibleIndex += l.size();

            key = params.key + 1;
        }

        callback.onResult(l, key);
    }
}
