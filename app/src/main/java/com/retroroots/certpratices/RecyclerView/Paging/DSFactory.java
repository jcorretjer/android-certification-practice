package com.retroroots.certpratices.RecyclerView.Paging;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

public class DSFactory extends DataSource.Factory
{
    private MutableLiveData<PageKeyedDataSource<Integer, Integer>> dataSourceLiveData = new MutableLiveData<>();

    public MutableLiveData<PageKeyedDataSource<Integer, Integer>> GetDataSourceLiveData()
    {
        return dataSourceLiveData;
    }

    @NonNull
    @Override
    public DataSource create()
    {
        PDataSource recPageDataSource = new PDataSource();

        dataSourceLiveData.postValue(recPageDataSource);

        return recPageDataSource;
    }


}
