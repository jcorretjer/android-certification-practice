package com.retroroots.certpratices.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.retroroots.certpratices.R;
import com.retroroots.certpratices.Room.REntity;

import java.util.List;

public class RAdapter extends RecyclerView.Adapter<RAdapter.RecViewHolder>
{
    private List<REntity> list;

    public RAdapter(List<REntity> list)
    {
        this.list = list;
    }

    public void SetList(List<REntity> list)
    {
        this.list = list;

        notifyDataSetChanged();
    }

    public static class RecViewHolder extends RecyclerView.ViewHolder
    {
        private TextView titleTxtVw;

        public RecViewHolder(@NonNull View itemView)
        {
            super(itemView);

            titleTxtVw = itemView.findViewById(R.id.recTitle);
        }
    }

    @NonNull
    @Override
    public RecViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new RecViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.child_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecViewHolder holder, int position)
    {
        holder.titleTxtVw.setText("Title " + list.get(position).getIndex());
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }
}
