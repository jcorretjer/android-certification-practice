package com.retroroots.certpratices.Room;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import java.util.List;

public class RRepo
{
    private static RDAO rdao;

    //private LiveData<List<REntity>> indexes;

    private LiveData<PagedList<REntity>> indexes;

    public RRepo(Context context)
    {
        rdao = RDB.GetInstance(context).dao();

        indexes = new LivePagedListBuilder<>(rdao.GetAll(), 10).build() ;

        //indexes = rdao.GetAll();
    }

    public void Insert(REntity entity)
    {
        new InsertThread(entity).start();
    }

    public LiveData<PagedList<REntity>> GetAll ()
    {
        return indexes;
    }

    /*public LiveData<List<REntity>> GetAll ()
    {
        return indexes;
    }*/

    private static class InsertThread extends Thread
    {
        private REntity entity;

        private InsertThread(REntity entity)
        {
            this.entity = entity;
        }

        @Override
        public void run()
        {
            rdao.Insert(this.entity);
        }
    }
}
