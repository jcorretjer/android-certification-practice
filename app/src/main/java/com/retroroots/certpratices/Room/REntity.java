package com.retroroots.certpratices.Room;

import androidx.lifecycle.ViewModel;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "AndroidCert")
public class REntity
{
    @PrimaryKey(autoGenerate = true)
    private long id;

    public REntity(int index)
    {
        this.index = index;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    private int index;

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }
}
