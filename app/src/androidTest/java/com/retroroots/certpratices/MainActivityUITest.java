package com.retroroots.certpratices;

import androidx.test.core.app.ActivityScenario;

import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

public class MainActivityUITest
{
    @Test
    public void TextChangeIsWorking()
    {
        ActivityScenario.launch(MainActivity.class);

        onView(withId(R.id.txtvw))
                .check(matches(withText("testing")));
    }
}