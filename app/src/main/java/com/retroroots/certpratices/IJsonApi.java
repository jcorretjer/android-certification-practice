package com.retroroots.certpratices;

import com.retroroots.certpratices.Models.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IJsonApi
{
    @GET("facts/random?animal_type=cat&amount=3")
    Call<List<Post>> getPost();
}
