package com.retroroots.certpratices.Models;

public class PStatus
{
    private boolean verified;

    public boolean getVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    private float sentCount;

    public float getSentCount() {
        return sentCount;
    }

    public void setSentCount(float sentCount) {
        this.sentCount = sentCount;
    }

    @Override
    public String toString()
    {
        return "PStatus" +
                "verified=" + verified +
                ", sentCount=" + sentCount;
    }
}
