package com.retroroots.certpratices;

import android.content.Context;
import android.util.AttributeSet;

public class CustomView extends androidx.appcompat.widget.AppCompatTextView
{
    public CustomView(Context context)
    {
        super(context);

        init();
    }

    public CustomView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        init();
    }

    public CustomView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);

        init();
    }

    private void init()
    {
        setText("Custom created view");
    }
}
